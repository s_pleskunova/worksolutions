<?php

use App\Controllers\AuthControllers;
use App\Controllers\ProfileControllers;
use App\Middleware\AuthMiddleware;

$app->map(['POST'], '/api/v1/login', AuthControllers::class . ':login');
$app->map(['GET'], '/api/v1/logout', AuthControllers::class . ':logout');
$app->map(['GET'], '/api/v1/profile/{id}', ProfileControllers::class . ':index')
    ->add(new AuthMiddleware());