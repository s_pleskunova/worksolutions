<?php

namespace App\Controllers;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface;

class ProfileControllers
{
    public function index(ServerRequestInterface $request, ResponseInterface $response, $args): Response
    {
        $response->getBody()->write(json_encode([
            'id' => $args ['id'],
            'name' => 'Tony',
            'gender' => 'male',
        ]));

        return $response->withHeader('Content-type', 'application/json');
    }
}