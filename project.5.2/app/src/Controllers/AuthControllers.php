<?php

namespace App\Controllers;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface;

class AuthControllers
{
    public function login(ServerRequestInterface $request, ResponseInterface $response, array $args): Response
    {
        $response
            ->getBody()
            ->write(
                json_encode(
                    ['data' => [
                        'token' => $_ENV['TOKEN'],
                        'userId' => rand(1000, 9999)
                    ]]
                )
            );

        return $response->withHeader('Content-Type', 'application/json');
    }

    public function logout(ServerRequestInterface $request, ResponseInterface $response, $args): Response
    {
        $response
            ->getBody()
            ->write(
                json_encode(
                    ['success' => true]
                ));

        return $response->withHeader('Content-type', 'application/json');
    }
}
