<?php

namespace App\Middleware;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;
use Slim\Psr7\Response;

class AuthMiddleware
{
    public function __invoke(Request $request, RequestHandler $handler)
    {
        if ($this->checkHeaders($request) == false) {
            $response = new Response();
            $response->getBody()->write(json_encode("Bad request"));
            return ($response)
                ->withStatus(400)
                ->withHeader('Content-type', 'application/json');
        }
        try {
            $client = new Client();
            $client->request('GET', 'http://postman-echo.com/basic-auth', [
                'headers' => [
                    'Authorization' => 'Basic' . ' ' . $request->getHeaderLine('Authorization')
                ]
            ]);

            return $handler->handle($request);

        } catch (ClientException $e) {
            $response = $e->getResponse();
            $response->getBody()->write("Unauthorized");

            return $response
                ->withStatus(401)
                ->withHeader('Content-type', 'application/json');
        }
    }

    public function checkHeaders(Request $request): bool
    {
        return $request->hasHeader('Authorization');
    }
}
