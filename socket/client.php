<?php

use App\Client;
use App\SocketClientConnect;
use Symfony\Component\Dotenv\Dotenv;

require_once __DIR__ . '/vendor/autoload.php';

$dotenv = new Dotenv();
$dotenv->load(__DIR__.'/.env');

$host = getenv('host');
$port = getenv('port');

$client = new Client(new SocketClientConnect($host, $port));
$client->message((string)$argv[1]);