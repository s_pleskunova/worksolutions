<?php

use App\Server;
use App\SocketServerConnect;
use Symfony\Component\Dotenv\Dotenv;

require_once __DIR__ . '/vendor/autoload.php';

$dotenv = new Dotenv();
$dotenv->load(__DIR__.'/.env');

$host = getenv('host');
$port = getenv('port');

$server = new Server(new SocketServerConnect($host, $port));
$server->run((string)$argv[1]);