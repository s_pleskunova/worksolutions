<?php

namespace App;

interface ConnectServerInterface
{
    public function read();
    public function write(string $msg): void;
    public function accept(): bool;
    public function bind(): bool;
    public function listen(): bool;
    public function closeAccept(): void;
    public function close(): void;
}