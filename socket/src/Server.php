<?php

namespace App;

class Server
{
    /**
     * @var ConnectServerInterface
     */
    private ConnectServerInterface $connectServer;

    public function __construct(ConnectServerInterface $connectServer)
    {
        $this->connectServer = $connectServer;
    }

    public function run(string $msg)
    {
        $this->connectServer->bind();
        $this->connectServer->listen();
        do {
            try {
                $this->serverConnect($msg);
            } catch (\Exception $e) {
                echo $e->getMessage();
            }
            $this->socketRead($msg);
        } while (true);
    }

    public function serverConnect(string $msg)
    {
        echo "Ожидаем клиента" . "\r\n";
        $this->connectServer->accept();
        echo $msg . "\r\n";
        $this->connectServer->write($msg);
    }

    public function socketRead(string $msg)
    {
        do {
            $read = $this->connectServer->read();
            if ($read === false) {
                echo "Ошибка: " . "\r\n";

            } else {
                if (trim($read) == "") return;
                else echo $read . "\r\n";
            }
            if ($read === 'exit') {
                $this->connectServer->closeAccept();
                $this->closeConnect();
                return;
            }
            echo $msg . "\r\n";
            $this->connectServer->write($read);
            echo "" . "\r\n";
        } while (true);
    }

    public function closeConnect()
    {
        echo "Закрываем соединение... " . "\n";
        $this->connectServer->close();
    }
}