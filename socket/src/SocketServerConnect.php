<?php

namespace App;

class SocketServerConnect implements ConnectServerInterface
{
    const maxByteForRead=1024;
    const backlog=10;
    /**
     * @var false|resource
     */
    private  $socket;
    private int $host;
    private int $port;
    /**
     * @var false|resource
     */
    private $accept;

    public function __construct(int $host, int $port)
    {
        $this->socket = socket_create(AF_UNIX, SOCK_STREAM, SOL_SOCKET);
        $this->host = $host;
        $this->port = $port;
        $this->accept = false;
    }

    public function bind(): bool
    {
        return socket_bind($this->socket, $this->host, $this->port);
    }

    public function listen(): bool
    {
        return socket_listen($this->socket, self::backlog);
    }

    public function accept(): bool
    {
        $this->accept = socket_accept($this->socket);

        return $this->accept !== false;
    }

    public function read(): void
    {
        socket_read($this->accept, self::maxByteForRead);
    }

    public function write(string $msg): void
    {
        socket_write($this->accept, $msg, strlen($msg));
    }

    public function closeAccept(): void
    {
        socket_close($this->accept);
    }

    public function close(): void
    {
        socket_close($this->socket);
    }
}