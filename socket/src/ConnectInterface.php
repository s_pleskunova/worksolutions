<?php

namespace App;

interface ConnectInterface
{
    public function open(): bool;
    public function close(): void;
    /**
     * @return false|string
     */
    public function read();
    /**
     * @param $msg
     * @return false|int|void
     */
    public function write(string $msg);
}