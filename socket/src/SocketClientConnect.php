<?php

namespace App;

class SocketClientConnect implements ConnectInterface
{
    const maxByteForRead=1024;
    /**
     * @var false|resource
     */
    private $socket;
    private int $host;
    private int $port;

    public function __construct(int $host, int $port)
    {
        $this->socket = socket_create(AF_UNIX, SOCK_STREAM, SOL_SOCKET);
        $this->host = $host;
        $this->port = $port;
    }

    public function open(): bool
    {
        return socket_connect($this->socket, $this->host, $this->port);
    }

    public function close(): void
    {
        socket_close($this->socket);
    }

    /**
     * @return false|string
     */
    public function read()
    {
        return socket_read($this->socket, self::maxByteForRead);
    }

    /**
     * @param $msg
     * @return false|int|void
     */
    public function write(string $msg)
    {
        return socket_write($this->socket, $msg, strlen($msg));
    }
}
