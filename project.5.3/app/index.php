<?php

use App\Components\UserManager;
use App\Components\MyDIContainer;

require_once __DIR__ . "/vendor/autoload.php";

$depends = include('di-config.php');
$container = new MyDIContainer($depends);

$userManager = $container->get(UserManager::class);
$userManager->register('sv@yandex.ru', 'pass');