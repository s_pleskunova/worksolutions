<?php

namespace App\Components\Test;

use App\Components\Mailer;
use App\Components\MyDIContainer;
use App\Components\UserManager;
use Exception;
use PHPUnit\Framework\TestCase;
use ReflectionClass;

class MyDIContainerTest extends TestCase
{
    private MyDIContainer $container;

    /**
     * @dataProvider providerData
     * @param $data
     * @throws Exception
     */
    public function testCreateInstance($data)
    {
        $this->container = new MyDIContainer();

        $this->assertInstanceOf($data, $this->container->get($data));
        $this->assertInstanceOf($data, $this->container->make($data));
    }

    public function providerData(): array
    {
        return [
            [
                UserManager::class,
                Mailer::class
            ],
        ];
    }

    public function testExceptionBind()
    {
        $this->container = new MyDIContainer();

        $this->expectException(Exception::class);
        $this->container->bind("User");
        $this->getReflector("User");
    }

    public function getReflector($name)
    {
        $this->container = new MyDIContainer();

        $class = new ReflectionClass(MyDIContainer::class);
        $method = $class->getMethod('getReflector');
        $method->setAccessible(true);
        $method->invokeArgs($this->container, $name);
    }
}
