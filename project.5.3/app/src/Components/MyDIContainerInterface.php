<?php

namespace App\Components;

use ReflectionClass;

interface MyDIContainerInterface
{
    public function bind(string $name): void;
    public function make(string $name);
    public function get(string $name);
}