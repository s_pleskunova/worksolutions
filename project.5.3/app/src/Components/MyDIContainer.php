<?php

namespace App\Components;

use Exception;
use ReflectionClass;

class MyDIContainer implements MyDIContainerInterface
{
    public array $storage = [];
    public array $instances = [];

    public function __construct(array $storage = [])
    {
        $this->storage = $storage;
    }

    /**
     * @param $name
     * @throws Exception
     */
    public function bind(string $name): void
    {
        if (!class_exists($name)) {
            throw new Exception("Class {$name} not found");
        }
        if (!in_array($name, $this->storage)) {
            array_push($this->storage, $name);
        }
    }

    private function getReflector(string $name): ReflectionClass
    {
        $reflector = new ReflectionClass($name);
        if (!$reflector->isInstantiable()) {
            throw new Exception("Class {$name} is not instantiable");
        }

        return $reflector;
    }

    /**
     * @param string $name
     * @return mixed
     * @throws Exception
     */
    public function make(string $name)
    {
        if (!isset($this->storage[$name])) {
            $this->bind($name);
        }
        $reflector = $this->getReflector($name);
        $constructor = $reflector->getConstructor();
        if (!is_null($constructor)) {
            $parameters = $constructor->getParameters();

            return $reflector->newInstanceArgs(
                array_map(fn(\ReflectionParameter $parameter) =>
                    $this->make($parameter->getClass()->getName()), $parameters)
            );
        }

        return $reflector->newInstance();
    }

    /**
     * @param $name
     * @return mixed
     * @throws Exception
     */
    public function get(string $name)
    {
        if (!array_key_exists($name, $this->instances)) {
            $this->instances[$name] = $this->make($name);
        }

        return $this->instances[$name];
    }
}