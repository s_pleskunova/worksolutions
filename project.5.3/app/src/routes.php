<?php

return [
    '/api/v1/auth' => [
        'method' => ['POST'],
        'action' => 'AuthController@index'
    ],
    '/api/v1/logout' => [
        'method' => ['GET'],
        'action' => 'AuthController@logout',
    ],
    '/api/v1/profile' => [
        'method' => ['GET'],
        'action' => 'ProfileController@index',
    ],
];