<?php

namespace App\Controllers;

class ProfileController
{
    public function index($id)
    {
        return [
            'id' => $id,
            'name' => 'Tony',
            'gender' => 'male',
        ];
    }
}