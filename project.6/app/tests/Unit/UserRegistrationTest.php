<?php

namespace App\tests\Unit\UserRegistrationTest;

use App\Models\User;
use App\Models\Role;
use App\Stories\UserRegistrationStory;
use Laravel\Lumen\Testing\DatabaseMigrations;
use TestCase;

class UserRegistrationTest extends TestCase
{
    use DatabaseMigrations;

    public function testRegistersAValidUser()
    {
        $role = Role::factory()->create();
        $user = User::factory()->make();
        $story = new UserRegistrationStory();
        $story->register([
            'name' => $user->name,
            'email' => $user->email,
            'password' => $user->password,
            'role_id' => $role->id,
        ]);
        $this->seeInDatabase('users', ['email' => $user->email]);
    }

    public function testRegistersAValidUserErr()
    {
        $role = Role::factory()->create();
        $user = User::factory()->make();
        $story = new UserRegistrationStory();
        $story->register([
            'name' => $user->name,
            'email' => 'someEmail',
            'password' => $user->password,
            'role_id' => $role->id,
        ]);
        $this->NotseeInDatabase('users', ['email' => '$user->email']);
    }
}
