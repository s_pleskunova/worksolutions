<?php

namespace App\tests\Features\UserAuthTest;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Laravel\Lumen\Testing\DatabaseMigrations;
use TestCase;

class UserAuthTest extends TestCase
{
    use DatabaseMigrations;

    public function testLogin()
    {
        $user = User::factory()->create();
        $response = $this->json('POST', 'login', [
            'email' => $user->email,
            'password' => Hash::make($user->password)
        ]);
        echo $user->password;
        $response->assertResponseStatus(401);
    }
}
