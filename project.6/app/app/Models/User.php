<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 *
 * @package App\Models
 */
class User extends Model
{
    use HasFactory;

    protected array $fillable = ['name', 'email', 'password'];

    public function role(): BelongsToMany
    {

        return $this->belongsToMany(Role::class, 'user_roles', 'user_id', 'role_id');
    }
}
