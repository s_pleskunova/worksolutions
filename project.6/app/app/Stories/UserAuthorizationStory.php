<?php

namespace App\Stories;

use App\Models\Role;
use App\Models\User;
use App\Models\UserRole;
use Firebase\JWT\JWT;
use Illuminate\Support\Facades\Hash;

class UserAuthorizationStory implements AuthorizationInterface
{
    public function generateJwt(User $user): string
    {
        $payload = [
            'id' => $user->id,
            'name' => $user->name,
            'email' => $user->email,
            'role' => $user->role()->value('role')
        ];

        return JWT::encode($payload, getenv('JWT_KEY'));
    }

    /***
     * @param array $data
     * @return string
     */

    public function auth(array $data): string
    {
        $user = User::where('email', $data['email'])->first();
        if (!$user) {
            return response()->json('Email does not exist.', 401);
        }
        if (Hash::check($data['password'], $user->password)) {
            return response()->json(['token' => $this->generateJwt($user)]);
        }

        return response()->json('Email or password is wrong.', 401);
    }
}
