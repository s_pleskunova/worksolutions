<?php

namespace App\Stories;

use App\Models\User;

interface UserRegistration
{
    public function register(array $registrationData): ?User;
}
