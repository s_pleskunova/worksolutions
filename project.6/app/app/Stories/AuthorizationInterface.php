<?php

namespace App\Stories;

use App\Models\User;

interface AuthorizationInterface
{
    public function generateJwt(User $user): string;
    public function auth(array $data): string;
}
