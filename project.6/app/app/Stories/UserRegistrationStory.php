<?php

namespace App\Stories;

use App\Models\Role;
use App\Models\User;
use Exception;
use Illuminate\Support\Facades\DB;

class UserRegistrationStory implements UserRegistration
{
    public function register(array $registrationData): ?User
    {
        try {
            DB::beginTransaction();

            $user = new User;
            $user->name = $registrationData['name'];
            $user->email = $registrationData['email'];
            $user->password = app('hash')->make($registrationData['password']);
            $user->save();

            $role = Role::find([$user->id, 1]);
            $user->role()->attach($role);

            DB::commit();

        } catch (Exception $e) {
            DB::rollback();
            echo $e->getMessage();
        }

        return $user;
    }
}
