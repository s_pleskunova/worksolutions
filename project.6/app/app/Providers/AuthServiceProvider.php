<?php

namespace App\Providers;

use App\Stories\AuthorizationInterface;
use App\Stories\UserAuthorizationStory;
use App\Stories\UserRegistration;
use App\Stories\UserRegistrationStory;
use App\Models\User;
use Illuminate\Support\ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        $this->app['auth']->viaRequest('api', function ($request) {
            if ($request->input('api_token')) {
                return User::where('api_token', $request->input('api_token'))->first();
            }
        });

        $this->app->bind(UserRegistration::class, UserRegistrationStory::class);
        $this->app->bind(AuthorizationInterface::class, UserAuthorizationStory::class);
    }
}
