<?php


namespace App\Http\Middleware;

use Exception;
use Firebase\JWT\JWT;
use Closure;
use Illuminate\Http\JsonResponse;

class AuthMiddleware
{
    public function handle($request, Closure $next)
    {
        $token = $request->header('X-Auth-Token');

        if (!$token) {
            return response('token missing', 401);
        }

        try {
            $decodeData = JWT::decode($token, getenv('JWT_KEY'), ['HS256']);
            $request->merge(['user' => $decodeData]);

            return $next($request);

        } catch (Exception $e) {

            return response('token not decrypted', 401);
        }
    }
}
