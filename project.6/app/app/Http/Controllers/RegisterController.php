<?php

namespace App\Http\Controllers;

use App\Stories\UserRegistration;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function register(Request $request, UserRegistration $story)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'email' => 'required|unique:users',
            'password' => 'required|string'
        ]);

        $registrationData = ([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => $request->input('password')
        ]);
        return $story->register($registrationData);
    }
}
