<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ProfileController
{
    public function profile(Request $request): JsonResponse
    {
        return response()->json($request->input('user'), 200);
    }
}
