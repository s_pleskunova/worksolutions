<?php

namespace App\Http\Controllers;

use App\Stories\AuthorizationInterface;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function login(Request $request, AuthorizationInterface $story)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $data = ([
            'email' => $request->input('email'),
            'password' => $request->input('password')
        ]);

        return $story->auth($data);
    }
}
