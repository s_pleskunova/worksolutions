<?php

namespace Database\Factories;

use App\Models\Role;
use Illuminate\Database\Eloquent\Factories\Factory;

class RoleFactory extends Factory
{
    protected string $model = Role::class;

    public function definition(): array
    {
        return [
            'role' => 'admin',
        ];
    }
}
