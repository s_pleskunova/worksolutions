<?php

use App\AnalyzerBanknote;
use App\Cell\FiveHundredCell;
use App\Cell\FiveThousandCell;
use App\Cell\OneHundredCell;
use App\Cell\OneThousandCell;
use App\Cell\TwoHundredCell;
use App\Cell\TwoThousandCell;
use App\SimpleATM;

require_once __DIR__ . '/vendor/autoload.php';

$cells = [
    OneHundredCell::class,
    TwoHundredCell::class,
    FiveHundredCell::class,
    OneThousandCell::class,
    TwoThousandCell::class,
    FiveThousandCell::class,
];

$atm = new SimpleATM(new AnalyzerBanknote());

foreach ($cells as $cellClass) {
    $atm->addCell(new $cellClass());
}

$response = $atm->depositMoney([5000, 5000, 2000, 1000, 1000, 500, 100, 200, 400, 50]);
echo "Response sum= " . $response->getSum() . "\n";
echo "Response UnacceptedNominal= " . print_r($response->getUnacceptedNominal()) . "\n";
echo "Remainder = " . $atm->showRemainder() . "\n";

$withdrawResponse = $atm->withdrawMoney(9000);
echo "Response Error =". assertEqual(count($withdrawResponse->getErrors()), 0) . "\n";
echo "Remainder is " . assertEqual($atm->showRemainder(), 5800) . "\n";
echo "-----------------------------------" . "\n";

$withdrawResponse = $atm->withdrawMoney(150);
echo "Response Error =". assertEqual(count($withdrawResponse->getErrors()), 1) . "\n";
echo "Remainder is " . assertEqual($atm->showRemainder(), 5800) . "\n";
echo "-----------------------------------" . "\n";

$withdrawResponse = $atm->withdrawMoney(500);
echo "Response Error =". assertEqual(count($withdrawResponse->getErrors()), 0) . "\n";
echo "Remainder is " . assertEqual($atm->showRemainder(), 5300) . "\n";
echo "-----------------------------------" . "\n";

$withdrawResponse = $atm->withdrawMoney(6500);
echo "Response Error =". assertEqual(count($withdrawResponse->getErrors()), 1) . "\n";
echo "Remainder is " . assertEqual($atm->showRemainder(), 5300) . "\n";
echo "-----------------------------------" . "\n";

function assertEqual($assertFrom, $assertTo)
{
    return assert($assertFrom === $assertTo) ? "true" : "false";
}