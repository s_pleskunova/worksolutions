<?php

namespace App\Banknote;

class OneHundredBanknote implements BanknoteInterface
{
    public static function getNominal(): int
    {
        return 100;
    }
}