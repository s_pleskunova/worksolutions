<?php

namespace App\Banknote;

class FiveHundredBanknote implements BanknoteInterface
{
    public static function getNominal(): int
    {
        return 500;
    }
}