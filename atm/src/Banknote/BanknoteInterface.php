<?php

namespace App\Banknote;

interface BanknoteInterface
{
    public static function getNominal(): int;
}
