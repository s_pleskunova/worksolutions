<?php

namespace App\Banknote;

class TwoHundredBanknote implements BanknoteInterface
{
    public static function getNominal(): int
    {
        return 200;
    }
}