<?php

namespace App\Banknote;

class FiveThousandBanknote implements BanknoteInterface
{
    public static function getNominal(): int
    {
        return 5000;
    }
}