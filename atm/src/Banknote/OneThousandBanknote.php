<?php

namespace App\Banknote;

class OneThousandBanknote implements BanknoteInterface
{
    public static function getNominal(): int
    {
        return 1000;
    }
}