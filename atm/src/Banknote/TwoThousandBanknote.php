<?php

namespace App\Banknote;

class TwoThousandBanknote implements BanknoteInterface
{
    public static function getNominal(): int
    {
        return 2000;
    }
}