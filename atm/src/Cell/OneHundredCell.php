<?php

namespace App\Cell;

use App\Banknote\OneHundredBanknote;

class OneHundredCell extends Cell
{
    public static function getBanknoteNominal(): int
    {
        return OneHundredBanknote::getNominal();
    }
}