<?php

namespace App\Cell;

use App\Banknote\BanknoteInterface;

interface CellInterface
{
    public function put(BanknoteInterface $banknote);

    /**
     * @return mixed
     */
    public function pop();
    public function getCountBanknotes(): int;
    public function getBalance(): int;
    public static function getBanknoteNominal(): int;
}