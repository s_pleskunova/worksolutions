<?php

namespace App\Cell;

use App\Banknote\OneThousandBanknote;

class OneThousandCell extends Cell
{
    public static function getBanknoteNominal(): int
    {
        return OneThousandBanknote::getNominal();
    }
}