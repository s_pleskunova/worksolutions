<?php

namespace App\Cell;

use App\Banknote\TwoThousandBanknote;

class TwoThousandCell extends Cell
{
    public static function getBanknoteNominal(): int
    {
        return TwoThousandBanknote::getNominal();
    }
}