<?php

namespace App\Cell;

use App\Banknote\TwoHundredBanknote;

class TwoHundredCell extends Cell
{
    public static function getBanknoteNominal(): int
    {
        return TwoHundredBanknote::getNominal();
    }
}