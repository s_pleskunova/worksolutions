<?php

namespace App\Cell;

use App\Banknote\FiveThousandBanknote;

class FiveThousandCell extends Cell
{
    public static function getBanknoteNominal(): int
    {
        return FiveThousandBanknote::getNominal();
    }
}