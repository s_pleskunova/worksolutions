<?php

namespace App\Cell;

use App\Banknote\BanknoteInterface;

abstract class Cell implements CellInterface
{
    protected array $banknotes = [];

    public function put(BanknoteInterface $banknote)
    {
        array_push($this->banknotes, $banknote);
    }

    /**
     * @return BanknoteInterface|null
     */
    public function pop()
    {
        if (count($this->banknotes) <= 0) {
            return null;
        }
        return array_pop($this->banknotes);
    }

    public function getCountBanknotes(): int
    {
        return count($this->banknotes);
    }

    public function getBalance(): int
    {
        return $this->getCountBanknotes() * $this->getBanknoteNominal();
    }
}