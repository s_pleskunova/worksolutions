<?php

namespace App\Cell;

use App\Banknote\FiveHundredBanknote;

class FiveHundredCell extends Cell
{
    public static function getBanknoteNominal(): int
    {
        return FiveHundredBanknote::getNominal();
    }
}