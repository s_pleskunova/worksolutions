<?php

namespace App;

use App\Cell\CellInterface;

interface ATMInterface
{
    public function withdrawMoney(int $sum): ResponseInterface;
    public function depositMoney(array $banknotes): ResponseInterface;
    public function showRemainder(): int;
    public function addCell(CellInterface $cell): void;
}