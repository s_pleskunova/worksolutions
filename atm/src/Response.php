<?php

namespace App;

class Response implements ResponseInterface
{
    private int $sum;
    private array $unacceptedNominal;
    private array $errors = [];
    private array $nominals;

    public function __construct(int $sum = 0, array $unacceptedNominal = [], array $nominals = [])
    {
        $this->sum = $sum;
        $this->unacceptedNominal = $unacceptedNominal;
        $this->nominals = $nominals;
    }

    /**
     * @return int
     */
    public function getSum(): int
    {
        return $this->sum;
    }

    /**
     * @return array
     */
    public function getUnacceptedNominal(): array
    {
        return $this->unacceptedNominal;
    }

    public function getErrors(): array
    {
        return $this->errors;
    }

    public function addError(string $message): void
    {
        array_push($this->errors, $message);
    }

    /**
     * @return array
     */
    public function getNominals(): array
    {
        return $this->nominals;
    }
}