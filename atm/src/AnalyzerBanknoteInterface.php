<?php

namespace App;

use App\Banknote\BanknoteInterface;

interface AnalyzerBanknoteInterface
{
    /**
     * @param int $banknote
     * @return BanknoteInterface|null
     */
    public function createBanknote(int $banknote);
    public static function getBills(): array;
}