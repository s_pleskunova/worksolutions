<?php

namespace App;

use App\Banknote\BanknoteInterface;
use App\Banknote\OneHundredBanknote;
use App\Banknote\OneThousandBanknote;
use App\Banknote\TwoHundredBanknote;
use App\Banknote\FiveHundredBanknote;
use App\Banknote\TwoThousandBanknote;
use App\Banknote\FiveThousandBanknote;

class AnalyzerBanknote implements AnalyzerBanknoteInterface
{
    /**
     * @param int $banknote
     * @return BanknoteInterface|null
     */
    public function createBanknote(int $banknote)
    {
        if (!array_key_exists($banknote, self::getBills())) {
            return null;
        }
        $banknoteClass = self::getBills()[$banknote];
        return new $banknoteClass();
    }

    public static function getBills(): array
    {
        return [
            OneHundredBanknote::getNominal() => OneHundredBanknote::class,
            TwoHundredBanknote::getNominal() => TwoHundredBanknote::class,
            FiveHundredBanknote::getNominal() => FiveHundredBanknote::class,
            OneThousandBanknote::getNominal() => OneThousandBanknote::class,
            TwoThousandBanknote::getNominal() => TwoThousandBanknote::class,
            FiveThousandBanknote::getNominal() => FiveThousandBanknote::class
        ];
    }
}