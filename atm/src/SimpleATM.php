<?php

namespace App;

use App\Cell\Cell;
use App\Cell\CellInterface;

class SimpleATM implements ATMInterface
{
    /**
     * @var CellInterface[]
     */
    private array $cells = [];
    private int $sum;
    private AnalyzerBanknoteInterface $analyzer;

    public function __construct(AnalyzerBanknoteInterface $analyzer)
    {
        $this->analyzer = $analyzer;
    }

    public function depositMoney(array $banknotes): ResponseInterface
    {
        $unacceptedNominal = [];
        $currentNominal = [];
        $currentSum = 0;
        foreach ($banknotes as $nominalBanknote) {
            $banknote = $this->analyzer->createBanknote($nominalBanknote);
            if ($banknote === null) {
                array_push($unacceptedNominal, $nominalBanknote);
                continue;
            }
            array_push($currentNominal, $nominalBanknote);
            $currentSum = array_sum($currentNominal);
            $this->cells[$banknote::getNominal()]->put($banknote);
            $this->recalculationSumATM();
        }

        return new Response($currentSum, $unacceptedNominal);
    }

    public function showRemainder(): int
    {
        return $this->sum;
    }

    public function withdrawMoney(int $sum): ResponseInterface
    {
        if (($sum > $this->getFulBalance()) or ($sum % 100 !== 0)) {
            $response = new Response();
            $response->addError('некорректная сумма');
            return $response;
        }
        $nominals = $this->getOptimalNominalSum($sum);
        $this->recalculationSumATM();

        return new Response($sum, [], $nominals);
    }

    /**
     * @param int $sum
     * @return array
     */
    public function getOptimalNominalSum(int $sum): array
    {
        $nominals = [];
        krsort($this->cells);
        /** @var Cell $cell */
        foreach ($this->cells as $cell) {
            $withdrawSum = 0;
            if ($sum < $cell::getBanknoteNominal()) {
                continue;
            }

            $minCountNominals = floor($sum / $cell::getBanknoteNominal());
            if ($minCountNominals > $cell->getCountBanknotes()) {
                $minCountNominals = $cell->getCountBanknotes();
            }

            while ($minCountNominals > 0) {
                $banknote = $cell->pop();
                array_push($nominals, $banknote::getNominal());
                $withdrawSum += $banknote::getNominal();
                $minCountNominals--;
            }
            $sum -= $withdrawSum;
        }

        return $nominals;
    }

    private function getFulBalance()
    {
        $fullSumm = 0;
        foreach ($this->cells as $cell) {
            $fullSumm += $cell->getBalance();
        }

        return $fullSumm;
    }

    private function recalculationSumATM()
    {
        $this->sum = $this->getFulBalance();
    }

    public function addCell(CellInterface $cell): void
    {
        $this->cells[$cell::getBanknoteNominal()] = $cell;
    }
}