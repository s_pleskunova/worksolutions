<?php

namespace App;

interface ResponseInterface
{
    public function getSum(): int;
    public function getUnacceptedNominal(): array;
    public function getNominals(): array;
    public function addError(string $message): void;
    public function getErrors(): array;
}