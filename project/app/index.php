<?php

use App\AppImpl;
use App\Router;

require_once __DIR__ . '/vendor/autoload.php';

$routing = include('src/routes.php');
$router = new Router($routing);
$app = new AppImpl($router);
$app->run();
