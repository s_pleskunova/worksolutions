<?php

namespace App;

interface AppInterface
{
    public function run(): void;
    public function handler(array $params): ?JsonResponse;
}