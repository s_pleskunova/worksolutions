<?php

namespace App;

interface RequestInterface
{
    public function getUrl(): string;
    public function getMethod(): string;
    public function getParams(): array;
}