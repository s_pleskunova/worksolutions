<?php

namespace App;

class JsonResponse implements JsonResponseInterface
{
    private int $status;
    private array $data;

    public function __construct(int $status, array $data)
    {
        $this->status = $status;
        $this->data = $data;
    }

    public function json(): string
    {
        header('Content-Type: application/json');
        http_response_code($this->status);
        return json_encode($this->data);
    }
}