<?php

namespace App;

class Request implements RequestInterface
{
    protected ?string $url;
    protected string $method;
    protected array $params;

    public function __construct()
    {
        $this->url = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        $this->method = $_SERVER['REQUEST_METHOD'];
        $this->params = array_values($_REQUEST);
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getMethod(): string
    {
        return $this->method;
    }

    public function getParams(): array
    {
        return $this->params;
    }
}