<?php

namespace App;

use App\Exceptions\NotAllowedResponse;
use App\Exceptions\NotFoundResponse;

class Router
{
    public array $routes;

    public function __construct(array $config)
    {
        $this->routes = $config;
    }

    /**
     * @param RequestInterface $request
     * @return array
     * @throws NotFoundResponse
     * @throws NotAllowedResponse
     */
    public function getRouteMatch(RequestInterface $request): array
    {
        $url = $request->getUrl();
        $route = null;
        foreach ($this->routes as $key => $params) {
            preg_match("~$key~", $url,$matches);
            if ($key == $url) {
                $route = $params;
                break;
            }
        }
        if ($route === null) {
            throw new NotFoundResponse('Page not found');
        }
        if (!in_array($request->getMethod(), $route['method'])) {
            throw new NotAllowedResponse('wrong method used');
        }

        return $route;
    }
}