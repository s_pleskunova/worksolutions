<?php

namespace App;

interface JsonResponseInterface
{
    public function json(): string;
}