<?php


namespace App;

use App\Exceptions\ResponseException;
use Throwable;

class AppImpl implements AppInterface

{
    const namespaceControllers = 'App\\Controllers\\';

    public Router $router;
    public Request $request;

    public function __construct(Router $router)
    {
        $this->router = $router;
        $this->request = new Request();
    }

    public function run(): void
    {
        try {
            $response = $this->handler($this->router->getRouteMatch($this->request));
        } catch (ResponseException $e) {
            $response = new JsonResponse($e->getStatus(), ['message' => $e->getMessage()]);
        } catch (Throwable $e) {
            $response = new JsonResponse($e->getCode(),['message' => $e->getMessage()]);
        }
        echo $response->json();
    }

    public function handler(array $params): ?JsonResponse
    {
        $controllerAction = explode("@", $params['action']);
        $classControllers = self::namespaceControllers . $controllerAction[0];
        $action = $controllerAction[1];
        if (method_exists($classControllers, $action)) {
            $controller = new $classControllers();
            return new JsonResponse(200, $controller->$action(...$this->request->getParams()));
        }
        return null;
    }
}