<?php

namespace App\Exceptions;

class NotFoundResponse extends ResponseException
{
    public function getStatus(): int
    {
        return 404;
    }
}