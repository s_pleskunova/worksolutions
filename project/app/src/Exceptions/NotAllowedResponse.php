<?php

namespace App\Exceptions;

class NotAllowedResponse extends ResponseException
{
    public function getStatus(): int
    {
        return 405;
    }
}