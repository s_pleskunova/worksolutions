<?php

namespace App\Exceptions;

use Exception;

abstract class ResponseException extends Exception
{
    abstract public function getStatus(): int;
}