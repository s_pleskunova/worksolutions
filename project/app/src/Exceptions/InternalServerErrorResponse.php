<?php

namespace App\Exceptions;

class InternalServerErrorResponse extends ResponseException
{
    public function getStatus(): int
    {
        return 500;
    }
}